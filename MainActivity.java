package com.example.androidmrhead10;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

    public class MainActivity extends AppCompatActivity {

        //Mendeklarasi variabel
        ImageView rambut, alis, kumis, janggut;
        CheckBox cekRambut, cekAlis, cekKumis, cekJanggut;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            //Variabel berisi nilai
            rambut = findViewById(R.id.rambut);
            alis = findViewById(R.id.alis);
            kumis = findViewById(R.id.kumis);
            janggut = findViewById(R.id.janggut);
            cekRambut = findViewById(R.id.cekRambut);
            cekAlis = findViewById(R.id.cekAlis);
            cekKumis = findViewById(R.id.cekKumis);
            cekJanggut = findViewById(R.id.cekJanggut);

            //Metode cek Rambut
            cekRambut.setOnClickListener(new View.OnClickListener() {

                //Menentukan Visibilitas Checkbox
                boolean visible;

                @Override
                public void onClick(View view) {
                    visible = !visible;
                    rambut.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });

            //Metode pengecekan Alis
            cekAlis.setOnClickListener(new View.OnClickListener() {

                //Menentukan Visibilitas Checkbox
                boolean visible;

                @Override
                public void onClick(View view) {
                    visible = !visible;
                    alis.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });

            //Metode pengecekan Kumis
            cekKumis.setOnClickListener(new View.OnClickListener() {

                //Menentukan Visibilitas Checkbox
                boolean visible;

                @Override
                public void onClick(View view) {
                    visible = !visible;
                    kumis.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });

            //Metode pengecekan Janggut
            cekJanggut.setOnClickListener(new View.OnClickListener() {

                //Menentukan Visibilitas Checkbox
                boolean visible;

                @Override
                public void onClick(View view) {
                    visible = !visible;
                    janggut.setVisibility(visible ? View.VISIBLE : View.GONE);
                }
            });
        }
    }
